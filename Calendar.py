# Make sure you are logged into your Monash student account.
# Go to: https://developers.google.com/calendar/quickstart/python
# Click on "Enable the Google Calendar API"
# Configure your OAuth client - select "Desktop app", then proceed
# Click on "Download Client Configuration" to obtain a credential.json file
# Do not share your credential.json file with anybody else, and do not commit it to your A2 git repository.
# When app is run for the first time, you will need to sign in using your Monash student account.
# Allow the "View your calendars" permission request.


# Students must have their own api key
# No test cases needed for authentication, but authentication may required for running the app very first time.
# http://googleapis.github.io/google-api-python-client/docs/dyn/calendar_v3.html


# Code adapted from https://developers.google.com/calendar/quickstart/python
from __future__ import print_function
import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# If modifying these scopes, delete the file token.pickle.
#SCOPES = ['https://www.googleapis.com/auth/calendar.readonly']
SCOPES = ['https://www.googleapis.com/auth/calendar']

def get_calendar_api():
    """
    Get an object which allows you to consume the Google Calendar API.
    You do not need to worry about what this function exactly does, nor create test cases for it.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)

    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    return build('calendar', 'v3', credentials=creds)

def get_events(api):
    """
    Shows basic usage of the Google Calendar API.
    Prints the start and name of the next n events on the user's calendar.
    """
    events_result = api.events().list(calendarId='primary', singleEvents=True,
                                      orderBy='startTime').execute()
    return events_result.get('items', [])

    # Add your methods here.

def delete(api, event_title, type):
    events = get_events(api)
    event_id = None
    for event in events:
        if event['summary'] == event_title:
            event_id = event['id']

    if event_id is not None:
        if type == 0:
            api.events().delete(calendarId='primary', eventId=event_id).execute()
            print("Event Deleted")
        elif type == 1:
            EVENT = {"reminders": {"overrides":[],"useDefault": True}}
            api.events().patch(calendarId='primary', eventId=event_id, body=EVENT).execute()
            print("Event Updated")
    else:
        print("Event not found")

def insert_event(api):
    event = {
        'summary':'finally1',
        'start':{
            'dateTime':'2015-10-10T08:00:00%s' % '-07:00'
            },
        'end':{
            'dateTime':'2015-10-10T09:00:00%s' % '-07:00'
            },
        "reminders": {
            "overrides": [ # If the event doesn't use the default reminders, this lists the reminders specific to the event, or, if not set, indicates that no reminders are set for this event. The maximum number of override reminders is 5.
                {
                    "minutes": 42,
                    "method": "popup",
                },
            ],
            "useDefault": False
            }
        }
    api.events().insert(calendarId='primary', sendNotifications=True, body=event).execute()

def get_events(api, past_year_num, future_year_num, date_now):
    date_now = date_now.replace(hour=0, minute=0, second=0, microsecond=0)
    time_before = date_now
    time_end = time_before + datetime.timedelta(days=1)
    if past_year_num < 5:
        past_year_num = 5
    if future_year_num < 2:
        future_year_num = 2
    try:
        time_before = time_before.replace(year = time_before.year - past_year_num)
    except ValueError:
        time_before = time_before.replace(year = time_before.year - past_year_num, day = time_before.day - 1)
    try:
        time_end = time_end.replace(year = time_end.year + future_year_num)
    except ValueError:
        time_end = time_end.replace(year = time_end.year + future_year_num, month = 3, day = 1)
    time_min = time_before.isoformat() + 'Z'
    time_max = time_end.isoformat() + 'Z'
    print(time_min, time_max)
    events_result = api.events().list(calendarId='primary', timeMin=time_min, timeMax=time_max, singleEvents=True, orderBy='startTime').execute()
    events = events_result.get('items', [])
    return events


def print_events(events):
    if not events:
        print('No upcoming events found.')
    for event in events:
        print("Event title:", event['summary'])
        print("Start time:", event['start'])
        print("End time:", event['end'])
        print("Reminder:", event['reminders'])
"""
def search_events(api, keyword_type, keyword_value):
    events_result = api.events().list(calendarId='primary', singleEvents=True, orderBy='startTime').execute()
    events = events_result.get('items', [])
    if keyword_type = "event-name":
        for event in events:
            if event['summary'] == keyword_value:
                print("Event found")
                break
    elif keyword_type = "event-time":
        for event in events:
            if event['start-time']
    elif keyword_type = "reminder-type":
    else:
        raise KeyError("The keyword type must be event name, event time or reminder type")
"""
def main():
    api = get_calendar_api()
    time_now = datetime.datetime.utcnow().isoformat() + 'Z'  # 'Z' indicates UTC time
    #time = datetime.datetime.now()
    #insert_event(api)

    #print_events(api,time_now)
    print_events(get_events(api, 5, 0, datetime.datetime.now()))
    # delete_event(api,time_now,"Event1")
    #patch_event(api, "testingreminder")
    #delete_reminder(api,"testingreminder")
    #print_events(api,time_now)
    #time = datetime.datetime.now()
    #time = time.replace(month = 2, day = 29)
    #get_past_events(api, 6)

if __name__ == "__main__":  # Prevents the main() function from being called by the test suite runner
    main()
