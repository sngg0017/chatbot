# Testing strategy using whitebox testing techniques:

retrieve_events_from_api():

We applied path coverage to test all the paths that this function can have. Since this function is not a complicated long code, it is possible to get 100% path coverage by using only two test cases.

-   One test case is to test that the API request call is successful and a list of events are returned without entering the except part of the code.
    
-   Another test case is to test if the API request call fails which raises an exception causing the function to enter the except branch.
    

  

get_events():

For this function, we applied branch coverage and line coverage. This is because there are many if statements and we used several try and except blocks as well. Therefore, for all possible outcomes of the decision blocks in this function, we will have at least one test case to cover it to achieve full branch coverage. As for all try and except blocks, we will have at least one test case that will fulfill the except condition to execute and test the codes within the except part. By doing so, we will ensure our line coverage to be 100%. If we do not have test cases to cover the except part, the statements within it will not be executed causing the line coverage to be less than 100%. The reason we did not apply path coverage is because the number of combinations are too big. The number of tests needed for full path coverage is not practical.

  

get_events_based_on_mode():

For this function, we applied path coverage since the code is not too complicated. There is only one if...elif...else statement in this function. Hence, we only need three test cases to cover all three paths.

-   One test case to test the execution when the if condition is true.
    
-   One test case to test the execution when the elif condition is true
    
-   One test case to test the execution when both if, elif conditions are false and the else branch is entered.
    

  

update_navigation():

For this function, although there are a lot of decision blocks including nested decision blocks, we still applied path coverage. We needed 13 test cases to achieve 100% path coverage which is still achievable. In addition, by achieving 100% path coverage, we achieved 100% branch coverage and line coverage. We even achieve 100% condition coverage in this case since there is no decision block which consists of several conditions. Therefore, we chose the path coverage strategy for this function.

  

navigate_events():

For this function, we applied several whitebox testing strategies. At first, we used line coverage strategy so that we can cover the whole error handling block (try and except block) including the except part. Then, we applied MD/DC (Modified condition/Decision Coverage) to test a decision statement which consists of three combinations of conditions.

  
  
  

The decision statement:

-   if (navigate_mode != 0  and navigate_mode != 1  and navigate_mode != 2):
    

  

The truth table and the output for each combinations:

| Tests | Condition 1 | Condition 2 | Condition 3 | Output
|---|---|---|---|---|
| 1 | T | T | T | T 
| 2 | T | T | F | F 
| 3 | T | F | T | F 
| 4 | T | F | F | F 
| 5 | F | T | T | F 
| 6 | F | T | F | F 
| 7 | F | F | T | F
| 8 | F | F | F | F

-   For condition 1, we select test case 1 and 5 as second and third conditions in both cases are the same while first condition and the output changes.
    
-   For condition 2, we select test case 1 and 3 as first and third conditions in both cases are the same while second condition and the output changes.
    
-   For condition 3, we select test case 1 and 2 as first and second conditions in both cases are the same while third condition and the output changes.
    
-   Therefore, we only need 4 test cases to satisfy the MC/DC coverage which are test cases 1,2,3 and 5.
    

  

Despite that, there is a while loop in this function, to test the while loop, we applied loop coverage. Therefore, we should have three cases where one will skip the loop, one will execute the loop once and one will execute the loop multiple times. However, in this implementation, it is impossible to skip the loop. Hence we eliminated that test case which results in a total of two test cases to test the loop.

  

search_events_and_reminders():

For this function, we applied several whitebox testing strategies. At first, we used a branch coverage strategy. There is one if...elif...else statement in this function. Therefore, for all possible outcomes of this decision block, we will have at least one test case to cover it to achieve full branch coverage. Then, we used line coverage strategy so that we can cover the except part in the error handling block since try and except is not considered as a decision statement. In addition, there is a for loop in this function. To test the loop, we already have test cases which will execute the loop several times. However, we did not have a test case to test the function when the loop is skipped, hence, we added a test case to cover this.

  
  
  

delete_event_or_reminder():

For this function, we applied branch coverage and line coverage. There is one if...elif...else statement in this function. Therefore, for all possible outcomes of this decision block, we will have at least one test case to cover it to achieve full branch coverage. As for all try and except blocks, we will have at least one test case that will fulfill the except condition to execute and test the codes within the except part. By doing so, we will ensure our line coverage to be 100%. In addition, there is a for loop in this function. To test the loop, we already have test cases which will execute the loop several times. However, we did not have a test case to test the function when the loop is skipped, hence, we added a test case to cover this.

  

print_events():

For this function, we applied branch and loop coverage. For the loop coverage, we have three test cases where one will skip the loop, one will execute the loop once and one will execute the loop multiple times. As for the branch coverage, although we have a decision statement, we do not have to add more test cases as the three cases used for loop coverage had covered the two possible outcomes of the decision block.

| Test Suite | Test case function name | Strategy applied | Case Description
|---|---|---|---|
| CalendarTestPrintEvents | test_print_no_event | Loop coverage | Skip the loop
| CalendarTestPrintEvents | test_print_one_event | Loop coverage | Execute the loop once
| CalendarTestPrintEvents | test_print_multiple_event | Loop coverage | Execute the loop multiple times
