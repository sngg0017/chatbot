import unittest
from unittest.mock import Mock
import Calendar
import datetime
# Add other imports here if needed


class CalendarTestGetEvents(unittest.TestCase):
    # This test tests number of upcoming events.
    def test_get_events_invalid_bounds(self):
        time_lower_bound = "2015-10-11T00:00:00Z"
        time_upper_bound = "2022-10-12T00:00:00Z"
        time_now = datetime.datetime.strptime("2020-10-11 00:00:00", '%Y-%m-%d %H:%M:%S')

        mock_api = Mock()
        Calendar.get_events(mock_api, 0, 0, time_now)
        self.assertEqual(
            mock_api.events.return_value.list.return_value.execute.return_value.get.call_count, 1)

        _, kwargs = mock_api.events.return_value.list.call_args_list[0]
        self.assertEqual(kwargs['timeMin'], time_lower_bound)
        self.assertEqual(kwargs['timeMax'], time_upper_bound)

    def test_get_events_leap_year_lower_bound(self):
        time_lower_bound = "2015-02-28T00:00:00Z"
        time_upper_bound = "2022-03-01T00:00:00Z"
        time_now = datetime.datetime.strptime("2020-02-29 00:00:00", '%Y-%m-%d %H:%M:%S')

        mock_api = Mock()
        Calendar.get_events(mock_api, 5, 2, time_now)
        self.assertEqual(
            mock_api.events.return_value.list.return_value.execute.return_value.get.call_count, 1)

        _, kwargs = mock_api.events.return_value.list.call_args_list[0]
        self.assertEqual(kwargs['timeMin'], time_lower_bound)
        self.assertEqual(kwargs['timeMax'], time_upper_bound)

    def test_get_events_leap_year_upper_bound(self):
        time_lower_bound = "2014-02-28T00:00:00Z"
        time_upper_bound = "2023-03-01T00:00:00Z"
        time_now = datetime.datetime.strptime("2020-02-28 00:00:00", '%Y-%m-%d %H:%M:%S')

        mock_api = Mock()
        Calendar.get_events(mock_api, 6, 3, time_now)
        self.assertEqual(
            mock_api.events.return_value.list.return_value.execute.return_value.get.call_count, 1)

        _, kwargs = mock_api.events.return_value.list.call_args_list[0]
        self.assertEqual(kwargs['timeMin'], time_lower_bound)
        self.assertEqual(kwargs['timeMax'], time_upper_bound)

def main():
    # Create the test suite from the cases above.
    suite = unittest.TestLoader().loadTestsFromTestCase(CalendarTestGetEvents)
    # This will run the test suite.
    unittest.TextTestRunner(verbosity=2).run(suite)


main()
