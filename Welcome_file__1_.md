# Testing strategy using whitebox testing techniques:
In this section, we have describe the whitebox testing techniques we used for each function we created. Each function will be separated into different subsections where the subsection title is the function name. In each subsection, there will be a table showing all the test cases created to test that function including which test suite was the test case placed, test case name, which strategy applied, and the description. 
## retrieve_events_from_api():

We applied path coverage to test all the paths that this function can have. Since this function is not a complicated long code, it is possible to get 100% path coverage by using only two test cases.

-   One test case is to test that the API request call is successful and a list of events are returned without entering the except part of the code.
    
-   Another test case is to test if the API request call fails which raises an exception causing the function to enter the except branch.

However, the first test case was eliminated. Many others test cases in other test suite has mock and used this function. In those test cases, they simulate that this function API request call is successful and a list of events was returned. Hence, we do not need to cover the first test case since it is being covered in other test cases. The second test case was included in CalendarTestAPI test suite as it is related to test API requests.
    
  Test case no. | Test Suite | Test case function name | Strategy applied | Case Description
|---|---|---|---|---|
1| CalendarTestAPI| test_retrieve_events_request_failed| Path coverage | Test retrieve events API request fails
  

## get_events():

For this function, we applied branch coverage and line coverage. This is because there are many if statements and we used several try and except blocks as well. Therefore, for all possible outcomes of the decision blocks in this function, we will have at least one test case to cover it to achieve full branch coverage. As for all try and except blocks, we will have at least one test case that will fulfill the except condition to execute and test the codes within the except part. By doing so, we will ensure our line coverage to be 100%. If we do not have test cases to cover the except part, the statements within it will not be executed causing the line coverage to be less than 100%. The reason we did not apply path coverage is because the number of combinations are too big. The number of tests needed for full path coverage is not practical. All the test cases were included in CalendarTestGetEvents test suite since it is related to get events functionality (user story 1 and 2).

Test case no. | Test Suite | Test case function name | Strategy applied | Case Description
|---|---|---|---|---|
2| CalendarTestGetEvents| test_get_events_invalid_bounds| Branch coverage | Test past year number <5 and future year number <2 |
3| CalendarTestGetEvents| test_get_events_leap_year_lower_bound| Line coverage | Test date now is a leap year and the year we want to set (in past) is not a leap year
4| CalendarTestGetEvents| test_get_events_leap_year_upper_bound| Line coverage | Test date now is a leap year and the year we want to set (in future) is not a leap year
5| CalendarTestGetEvents| test_get_events_with_invalid_past_year_num| Line coverage | Test invalid input for past year number
6| CalendarTestGetEvents| test_get_events_with_invalid_future_year_num| Line coverage | Test invalid input for future year number
7| CalendarTestGetEvents| test_get_events_with_invalid_date_now| Line coverage | Test invalid input for date now
8| CalendarTestGetEvents| test_get_events_with_invalid_past_num_valid_future_num| Black box testing | Test past year number < 5 and future year number >= 2
9| CalendarTestGetEvents| test_get_events_with_valid_past_num_invalid_future_num| Black box testing | Test past year number >= 5 and future year number < 2
  
  

## get_events_based_on_mode():

For this function, we applied path coverage since the code is not too complicated. There is only one if...elif...else statement in this function. Hence, we only need three test cases to cover all three paths.

-   One test case to test the execution when the if condition is true.
    
-   One test case to test the execution when the elif condition is true
    
-   One test case to test the execution when both if, elif conditions are false and the else branch is entered.

All the test cases were included in CalendarTestNavigate test suite since it is related to navigate events functionality.
    
 Test case no. | Test Suite | Test case function name | Strategy applied | Case Description
|---|---|---|---|---|
10| CalendarTestNavigate| test_get_events_mode_0| Path coverage | Test mode = 0
11| CalendarTestNavigate| test_get_events_mode_1| Path coverage | Test mode = 1
12| CalendarTestNavigate| test_get_events_mode_2| Path coverage | Test mode = 2
  

## update_navigation():

For this function, although there are a lot of decision blocks including nested decision blocks, we still applied path coverage. We needed 13 test cases to achieve 100% path coverage which is still achievable. In addition, by achieving 100% path coverage, we achieved 100% branch coverage and line coverage. We even achieve 100% condition coverage in this case since there is no decision block which consists of several conditions. Therefore, we chose the path coverage strategy for this function. All the test cases were included in CalendarTestNavigate test suite since it is related to navigate events functionality.

 Test case no. | Test Suite | Test case function name | Strategy applied | Case Description
|---|---|---|---|---|
13| CalendarTestNavigate| test_update_navigation_change_mode_0| Path coverage | Test mode = 0, user_input = 'd'
14| CalendarTestNavigate| test_update_navigation_change_mode_1| Path coverage | Test mode = 1, user_input = 'd'
15| CalendarTestNavigate| test_update_navigation_change_mode_2| Path coverage | Test mode = 2, user_input = 'd'
16| CalendarTestNavigate| test_update_navigation_previous_year| Path coverage | Test mode = 0, user_input = 'a'
17| CalendarTestNavigate| test_update_navigation_next_year| Path coverage | Test mode = 0, user_input = 's'
18| CalendarTestNavigate| test_update_navigation_previous_month_boundary| Path coverage | Test mode = 1, user_input = 'a' and month is 1
19| CalendarTestNavigate| test_update_navigation_previous_month_normal| Path coverage | Test mode = 1, user_input = 'a' and month is not 1
20| CalendarTestNavigate| test_update_navigation_next_month_boundary| Path coverage | Test mode = 1, user_input = 's' and month is 12
21| CalendarTestNavigate| test_update_navigation_next_month_normal| Path coverage | Test mode = 1, user_input = 's' and month is not 12
22| CalendarTestNavigate| test_update_navigation_previous_day| Path coverage | Test mode = 2, user_input = 'a'
23| CalendarTestNavigate| test_update_navigation_next_day| Path coverage | Test mode = 2, user_input = 's'
24| CalendarTestNavigate| test_update_navigation_quit_mode_0| Path coverage | Test mode = 0, user_input = 'q'
25| CalendarTestNavigate| test_update_navigation_quit_mode_1| Black box testing | Test mode = 1, user_input = 'q'
26| CalendarTestNavigate| test_update_navigation_quit_mode_2| Black box testing | Test mode = 2, user_input = 'q'
27| CalendarTestNavigate| test_update_navigation_invalid_input| Path coverage | Test user_input not 'q' or 'a' or 's' or 'd'

## navigate_events():

For this function, we applied several whitebox testing strategies. At first, we used line coverage strategy so that we can cover the whole error handling block (try and except block) including the except part. Then, we applied MD/DC (Modified condition/Decision Coverage) to test a decision statement which consists of three combinations of conditions.

The decision statement:
-   if (navigate_mode != 0  and navigate_mode != 1  and navigate_mode != 2):
    
The truth table and the output for each combinations:

| Tests | Condition 1 | Condition 2 | Condition 3 | Output |
|---|---|---|---|---|
| 1 | T | T | T | T |
| 2 | T | T | F | F |
| 3 | T | F | T | F |
| 4 | T | F | F | F |
| 5 | F | T | T | F |
| 6 | F | T | F | F |
| 7 | F | F | T | F |
| 8 | F | F | F | F |

-   For condition 1, we select test case 1 and 5 as second and third conditions in both cases are the same while first condition and the output changes.
    
-   For condition 2, we select test case 1 and 3 as first and third conditions in both cases are the same while second condition and the output changes.
    
-   For condition 3, we select test case 1 and 2 as first and second conditions in both cases are the same while third condition and the output changes.
    
-   Therefore, we only need 4 test cases to satisfy the MC/DC coverage which are test cases 1,2,3 and 5.

Despite that, there is a while loop in this function, to test the while loop, we applied loop coverage. Therefore, we should have three cases where one will skip the loop, one will execute the loop once and one will execute the loop multiple times. However, in this implementation, it is impossible to skip the loop. Hence we eliminated that test case which results in a total of two test cases to test the loop.

All the test cases were included in CalendarTestNavigate test suite since it is related to navigate events functionality.

  Test case no.| Test Suite | Test case function name | Strategy applied | Case Description
|---|---|---|---|---|
28| CalendarTestNavigate| test_navigate_events_invalid_navigate_mode| Line coverage | Test invalid input for navigate mode
29| CalendarTestNavigate| test_navigate_events_invalid_date| Line coverage | Test Test invalid input for date
30| CalendarTestNavigate| test_navigate_events_mode_three_conditions_true| MC/DC | Test first, second and third conditions true (output is true)
31| CalendarTestNavigate| test_navigate_events_mode_third_condition_false| MC/DC | Test first, second conditions true and third condition false (output is false)
32| CalendarTestNavigate| test_navigate_events_mode_second_condition_false| MC/DC | Test first, third conditions true and second condition false (output is false)
33| CalendarTestNavigate| test_navigate_events_mode_first_condition_false| MC/DC | Test second, third conditions true and first condition false (output is false)
34| CalendarTestNavigate| test_navigate_events_enter_loop_once| Loop coverage | Test execute the loop once
35| CalendarTestNavigate| test_navigate_events_enter_loop_multiple_times| Loop coverage | Test execute the loop multiple times
  

## search_events_and_reminders():

For this function, we applied several whitebox testing strategies. At first, we used a branch coverage strategy. There is one if...elif...else statement in this function. Therefore, for all possible outcomes of this decision block, we will have at least one test case to cover it to achieve full branch coverage. Then, we used line coverage strategy so that we can cover the except part in the error handling block since try and except is not considered as a decision statement. In addition, there is a for loop in this function. To test the loop, we already have test cases which will execute the loop several times. However, we did not have a test case to test the function when the loop is skipped, hence, we added a test case to cover this. All the test cases were included in CalendarTestSearch test suite since it is related to search events functionality.

 Test case no. | Test Suite | Test case function name | Strategy applied | Case Description
|---|---|---|---|---|
36| CalendarTestSearch| test_search_with_title_keyword| Branch coverage | Test type = 0
37| CalendarTestSearch| test_search_with_description_keyword| Branch coverage | Test type = 1
38| CalendarTestSearch| test_search_keyword_not_found| Branch coverage | Test type = 0, no events matches keyword
39| CalendarTestSearch| test_search_with_invalid_specified_search_keyword_type| Branch coverage | Test type != 0 and type != 1
40| CalendarTestSearch| test_search_with_invalid_type| Line coverage | Test invalid input for search type
41| CalendarTestSearch| test_search_skip_loop| Loop coverage | Test no events, skip loop
  
  

## delete_event_or_reminder():

For this function, we applied branch coverage and line coverage. There is one if...elif...else statement in this function. Therefore, for all possible outcomes of this decision block, we will have at least one test case to cover it to achieve full branch coverage. As for all try and except blocks, we will have at least one test case that will fulfill the except condition to execute and test the codes within the except part. By doing so, we will ensure our line coverage to be 100%. In addition, there is a for loop in this function. To test the loop, we already have test cases which will execute the loop several times. However, we did not have a test case to test the function when the loop is skipped, hence, we added a test case to cover this. Two test cases were included in CalendarTestAPI test suite as there are related to test API requests. The remaining test cases were included in CalendarTestDelete test suite since it is related to test the delete event or reminder functionality.

Test case no. | Test Suite | Test case function name | Strategy applied | Case Description
|---|---|---|---|---|
42| CalendarTestDelete| test_delete_event| Branch coverage | Test event found, type = 0
43| CalendarTestDelete| test_delete_reminder| Branch coverage | Test event found, type = 1
44| CalendarTestDelete| test_delete_not_found_event| Branch coverage | Test event not found
45| CalendarTestDelete| test_delete_invalid_type| Branch coverage | Test event found, type != 0 and type != 1
46| CalendarTestDelete| test_delete_skip_loop| Loop coverage | Test no events, skip loop
47| CalendarTestDelete| test_delete_with_invalid_type| Line coverage | Test invalid input for delete type
48| CalendarTestAPI| test_delete_event_request_failed| Line coverage | Test delete API request fails
49| CalendarTestAPI| test_update_event_request_failed| Line coverage | Test update API request fails
  

## print_events():

For this function, we applied branch and loop coverage. For the loop coverage, we have three test cases where one will skip the loop, one will execute the loop once and one will execute the loop multiple times. As for the branch coverage, although we have a decision statement, we do not have to add more test cases as the three cases used for loop coverage had covered the two possible outcomes of the decision block. All the test cases were included in CalendarTestPrintEvents test suite since it is related to test the print events functionality.

Test case no. | Test Suite | Test case function name | Strategy applied | Case Description
|---|---|---|---|---|
50| CalendarTestPrintEvents | test_print_no_event | Loop coverage | Test skip loop
51| CalendarTestPrintEvents | test_print_one_event | Loop coverage | Test execute the loop once
52| CalendarTestPrintEvents | test_print_multiple_event | Loop coverage | Test execute the loop multiple times


# Testing strategy using blackbox testing techniques:
In this section, we have describe the blackbox testing techniques we used for each user story. User stories will be separated into different subsections where the subsection title is the related user story. In each subsection, we will state the test cases we need when using blackbox testing, Then we will eliminate those test cases which are already covered by test cases generated by whitebox testing. If there are test cases which are not covered, we will add more new test cases. However, the test cases generated by using blackbox testing were included in the tables shown in whitebox testing section with the strategy applied column value to be "Black box testing".

## User Story 1 & 2
For this user story, if the user wish to see events and reminders for a certain time range, they will have to indicate two integer string values. The first integer string value is how many years in past does the user wish to see and the second integer string value is how many years in the future does the user wish to see. Therefore, there are two inputs. We used the category partition strategy and we determine that there are two categories:
- past year number 
	- choices: 
		- A: non integer string value (exceptional case)
		- B: integer string value that is less than 5
		- C: integer string value that is more than or equal to 5
- future year number
	- choices:
		- A: non integer string value (exceptional case)
		- B: integer string value that is less than 2
		- C: integer string value that is more than or equal to 2

Therefore, the test cases that we need are (where the each exceptional case is covered once only):
- (1): past year number A and future year number C (test case 5)
- (2): past year number B and future year number B (test case 2)
- (3): past year number B and future year number C (test case 8)
- (4): past year number C and future year number B (test case 9)
- (5): past year number C and future year number C (test case 3, 4)
- (6): past year number C and future year number A (test case 6)
	
From all 6 scenarios stated above, 1, 2, 5 and 6 were covered in test cases generated by applying whitebox testing. Therefore, we do not need add more test cases as they would be redundant. However, scenario 3 and 4 were not covered by whitebox testing. Hence, we added test case 8 and 9 for these 2 scenarios respective. 
 
## User Story 3
For this user story, if the user wish to navigate the calendar through different days, months and years to view all events within the time range, they will have to indicate three values. The first value is an integer string value indicating the navigation mode, the second value is a string indicating the date which the user wish to navigate from in the format of "YYYY-MM-DD" and the third is a character indicating an action. Therefore, there are three inputs. We used the category partition strategy and we determine that there are three categories:
- navigation mode
	- choices: 
		- A: non integer string value (exceptional case)
		- B: integer string value of 0
		- C: integer string value of 1
		- D: integer string value of 2
		- E: integer string value of not 0 or 1 or 2 (exceptional case)
- date
	- choices:
		- A: date string with format "YYYY-MM-DD"
		- B: date string with format that is not "YYYY-MM-DD" (exceptional case)
- user option
	- choices:
		- A: 'd'
		- B: 'a'
		- C: 's'
		- D: 'q'

The exceptional case for navigation mode choice A is covered in test case 28 while the exceptional case for navigation mode choice E is covered in test cases 30, 31, 32 and 33 (generated by using MC/DC). The exceptional case for date choice B is covered in test case 29. After handling the exceptional case, we have three choices for navigation mode, one choice for date and four user options.  Hence, we will need 12 test cases as follows:
- (1): navigation mode B and user option A (test case 13)
- (2): navigation mode B and user option B (test case 16)
- (3): navigation mode B and user option C (test case 17)
- (4): navigation mode B and user option D (test case 24)
- (5): navigation mode C and user option A (test case 14)
- (6): navigation mode C and user option B (test case 18, 19)
- (7): navigation mode C and user option C (test case 20, 21)
- (8): navigation mode C and user option D (test case 25)
- (9): navigation mode D and user option A (test case 15)
- (10): navigation mode D and user option B (test case 22)
- (11): navigation mode D and user option C (test case 23)
- (12): navigation mode D and user option D (test case 26)

From all 12 scenarios stated above, only scenarios 8 and 12 were not covered by the test cases generated by using whitebox testing (path coverage). Hence, we added test case 25 and 26 for these 2 scenarios respective.


## User Story 4
For this user story, if the user wish to search events and reminders using different key words, they will have to indicate two values. The first value is a string value indicating the keyword to search against and the second value is an integer string value indicating whether to search by event title or description. Therefore, there are two inputs. However, for the keyword, a string will be inputted by the user and we do not care whether it is empty or include special characters or not. Therefore, we only need to test the second input which is the search type. Since there is only one input we are concerned, we used the equivalence partitioning strategy. We have four partitions: 
- non integer string value (test case 40)
- integer string value of 0 (test case 36)
- integer string value of 1 (test case 37)
- integer string value of not 0 or 1 (test case 39)

Hence, we will need four test cases to test all four partitions. However, we have test case 36, 37, 39 and 40 which are generated using whitebox testing to cover the four partitions. Therefore, we do not need to add more test case since it will only be redundant.

## User Story 5
For this user story, if the user wish to delete event and reminder, they will have to indicate two values. The first value is a string value indicating the event title to be deleted or the event title of the reminder to be deleted. The second value is an integer string value indicating whether to delete the event or the reminder in the event. Therefore, there are two inputs. However, for the event title, a string will be inputted by the user and we do not care whether it is empty or include special characters or not. Therefore, we only need to test the second input which is the delete type. Since there is only one input we are concerned, we used the equivalence partitioning strategy. We have four partitions: 
- non integer string value (test case 47)
- integer string value of 0 (test case 42)
- integer string value of 1 (test case 43)
- integer string value of not 0 or 1 (test case 45)

Hence, we will need four test cases to test all four partitions. However, we have test case 42, 43, 45and 47which are generated using whitebox testing to cover the four partitions. Therefore, we do not need to add more test case since it will only be redundant.

